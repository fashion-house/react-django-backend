from django.contrib import admin
from api.models import Comment, Images, Product, CheckoutProduct, Order



@admin.register(Product)
class ProducModel(admin.ModelAdmin):
    list_filter = ('title',)

@admin.register(CheckoutProduct)
class CheckoutProductModel(admin.ModelAdmin):
    list_filter = ('title',)
    
@admin.register(Order)
class OrderModel(admin.ModelAdmin):
    list_filter = ('orderNumber',"isPayed")

@admin.register(Images)
class OrderModel(admin.ModelAdmin):
    list_filter = ('title',)

@admin.register(Comment)
class OrderModel(admin.ModelAdmin):
    list_filter = ('email', )
