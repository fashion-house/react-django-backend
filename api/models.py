from django.db import models

# Create your models here.
def upload_path(instance, filename):
    return "/".join( [filename])


class Images(models.Model):
    title = models.CharField(max_length=100)
    img1Seo = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img2Seo = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img1 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img2 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img3 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img4 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img5 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img6 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    img7 = models.ImageField(blank=True, null=True, upload_to=upload_path)
    def __str__(self):
        return self.title
class Comment(models.Model):
    productCode = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    name_surname = models.CharField(max_length=100)
    commentText = models.CharField(max_length=700)
    
    def __str__(self):
        return self.email

class Product(models.Model):
    title = models.CharField(max_length=100)
    desc = models.CharField(max_length=500)
    mainprice = models.CharField(max_length=500)
    price = models.CharField(max_length=500)
    views = models.FloatField(default=0)
    category = models.CharField(max_length=500)
    productSellAmount = models.FloatField(default=0)
    images = models.ManyToManyField(Images)
    # comments = models.ManyToManyField(Comment, blank=True, null=True )
    
    def __str__(self):
        return self.title

class CheckoutProduct(models.Model):
    orderNumber = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    price = models.CharField(max_length=500)
    productAmount = models.CharField(max_length=500)
    totalPrice = models.CharField(max_length=500)
    date = models.CharField(max_length=500)
    id = models.CharField(primary_key=True, max_length=500, unique=True)


    
    def __str__(self):
        return self.title + " " + self.orderNumber + " " + self.price + " " + self.productAmount

class Order(models.Model):
    orderNumber = models.CharField(max_length=100)
    name_surname = models.CharField(max_length=100)
    email = models.CharField(max_length=500)
    telephone = models.CharField(max_length=500)
    address = models.CharField(max_length=500)
    city = models.CharField(max_length=500)
    postcode = models.CharField(max_length=500)
    all_total_price = models.CharField(max_length=500)
    date = models.CharField(max_length=500)
    isPayed = models.BooleanField(null=True, blank=True)
    products = models.ManyToManyField(CheckoutProduct)
    id= models.CharField(max_length=100, primary_key=True, unique=True)
    shipmentState = models.CharField(max_length=500, default="Sipariş Alındı")


    
    def __str__(self):
         if self.isPayed: return  self.orderNumber + " " + self.name_surname + " " + self.date + " " + self.city + " " + self.all_total_price + " " + "Ödendi"
         elif self.isPayed == False: return  self.orderNumber + " " + self.name_surname + " " + self.date + " " + self.city + " " + self.all_total_price + " " + self.telephone + " " + self.postcode + " " + " " +  "Ödenmedi"
