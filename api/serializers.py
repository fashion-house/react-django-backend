from rest_framework import serializers
from .models import Comment, Images, Product, CheckoutProduct, Order
from django.contrib.auth.models import User
from rest_framework.authtoken.views import Token



class ImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Images
        fields = ["id", "title", "img1Seo" , "img2Seo" ,"img1", "img2", "img3", "img4","img5","img6","img7"]

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ["id", "email", "name_surname", "commentText","productCode"]

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', "title", "desc","mainprice", "price", "views", "category", "productSellAmount", "images" ]

class CheckoutSerializer(serializers.ModelSerializer):

    class Meta:
        model = CheckoutProduct
        fields = ['id', 'title',  'price', 'totalPrice', "productAmount", "orderNumber", "date" ]

class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['id', 'name_surname' ,"email","orderNumber", "date", "telephone", "address", "city", "postcode", "products", "all_total_price", "isPayed", "shipmentState" ]





class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']

        extra_kwargs= {'password' : {
            'write_only': True,
            'required' : True
        }}
        

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

