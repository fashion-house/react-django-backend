

from api.models import Comment, Images, Product, CheckoutProduct, Order
from .serializers import CommentSerializer, ImageSerializer, ProductSerializer, UserSerializer, CheckoutSerializer, OrderSerializer

from rest_framework import viewsets

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from django.contrib.auth.models import User






class ImageViewSet(viewsets.ModelViewSet):
    queryset= Images.objects.all()
    serializer_class= ImageSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes= (TokenAuthentication,)

class CommentsViewSet(viewsets.ModelViewSet):
    queryset= Comment.objects.all()
    serializer_class= CommentSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes= (TokenAuthentication,)

class ProductViewSet(viewsets.ModelViewSet):
    queryset= Product.objects.all()
    serializer_class= ProductSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes= (TokenAuthentication,)

class CheckoutViewSet(viewsets.ModelViewSet):
    queryset= CheckoutProduct.objects.all()
    serializer_class= CheckoutSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes= (TokenAuthentication,)

class OrderViewSet(viewsets.ModelViewSet):

    queryset= Order.objects.all()
    serializer_class= OrderSerializer
    # permission_classes = [IsAuthenticated]
    # authentication_classes= (TokenAuthentication,)


class UserViewSet(viewsets.ModelViewSet):
    queryset= User.objects.all()
    serializer_class= UserSerializer
