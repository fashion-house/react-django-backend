from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Product(models.Model):
    name=models.CharField(max_length=100)
    price=models.DecimalField(max_digits=10, decimal_places=2)
    # product_image=models.ImageField(upload_to="thumbnail")
    book_url=models.URLField()
    def __str__(self):
        return self.name
# class ProductPrice(models.Model):
#     paidPrice= models.CharField(max_length=100)
    # id= models.CharField(max_length=100, editable=False, primary_key=True)
    # surname= models.CharField(max_length=100)
    # email= models.CharField(max_length=100)
    # identityNumber= models.CharField(max_length=100)
    # lastLoginDate= models.CharField(max_length=100)
    # registrationDate= models.CharField(max_length=100)
    # registrationAddress= models.CharField(max_length=100)
    # ip= models.CharField(max_length=100)
    # city= models.CharField(max_length=100)
    # country= models.CharField(max_length=100)
    # zipCode= models.CharField(max_length=100)

# class Address(models.Model):
#     contactName= models.CharField(max_length=100)
#     city= models.CharField(max_length=100)
#     country= models.CharField(max_length=100)
#     address= models.CharField(max_length=100)
#     zipCode= models.CharField(max_length=100)

# class BasketItem(models.Model):
#     id= models.CharField(max_length=100, primary_key=True)
#     name= models.CharField(max_length=100)
#     category1= models.CharField(max_length=100)
#     category2= models.CharField(max_length=100)
#     itemType= models.CharField(max_length=100)
#     price= models.CharField(max_length=100)

# class BasketItems(models.Model):
#     basket_items=models.ManyToManyField(BasketItem)

# class Request (models.Model):
#     locale= models.CharField(max_length=100)
#     conversationId= models.CharField(max_length=100)
#     price= models.CharField(max_length=100)
#     paidPrice= models.CharField(max_length=100)
#     currency= models.CharField(max_length=100)
#     basketId= models.CharField(max_length=100)
#     paymentGroup= models.CharField(max_length=100)
#     callbackUrl= models.CharField(max_length=100)
#     # enabledInstallments= models.CharField(max_length=100)
#     buyer= models.ForeignKey(Buyer, on_delete=models.CASCADE)
#     shippingAddress= models.ForeignKey(Address, on_delete=models.CASCADE)
#     # billingAddress= models.ForeignKey(Address, on_delete=models.CASCADE)
#     basketItems= models.ForeignKey(BasketItems, on_delete=models.CASCADE)





