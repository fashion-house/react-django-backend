from django.urls import path
from .views import    IyzicoCheckoutView, result
from django.views.decorators.csrf import csrf_exempt


urlpatterns = [
    path('checkout/', IyzicoCheckoutView, name='iyzico_session'),
    # path('product/<int:pk>/', ProductPreview.as_view(), name="product"),

    # path('getorder/', GetOrder.as_view()),
    path('result/', result, name='result'),
    # path('productprice/', PriceViewSet.as_view({'get': 'list'})),


]