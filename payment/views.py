import iyzipay
from django.conf import settings
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import redirect

from django.shortcuts import render
import json
# Create your views here.
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt



from api.models import CheckoutProduct, Order
import uuid
import jquery


sozlukToken = list()
orderNumber = list()



options = {
    'api_key': settings.IYZICO_KEY_API_KEY,
    'secret_key': settings.IYZICO_KEY_SECRET_KEY,
    'base_url': settings.IYZICO_KEY_BASE_URL
}




@csrf_exempt
def IyzicoCheckoutView(request):       
    orderNumberPk =request.POST["order"]
    productIds =  request.POST["products"].split(",")
    orderNumber.append(orderNumberPk)
    querysetOrder = Order.objects.get(pk= orderNumberPk)
    map_iterator = map(lambda x: CheckoutProduct.objects.get(pk= x), productIds)


    

    buyer = {
    'id': querysetOrder.id,
    'name': querysetOrder.name_surname,
    'surname': '-',
    'gsmNumber': f"{querysetOrder.telephone}",
    'email': querysetOrder.email,
    'identityNumber': '74300864791',
    'lastLoginDate': '2015-10-05 12:43:35',
    'registrationDate': '2013-04-21 15:12:09',
    'registrationAddress': querysetOrder.address,
    'ip': '85.34.78.112',
    'city': querysetOrder.city,
    'country': 'Turkey',
    'zipCode': f"{querysetOrder.postcode}",
}

    address = {
    'contactName': querysetOrder.name_surname,
    'city': querysetOrder.city,
    'country': 'Turkey',
    'address': querysetOrder.address,
    'zipCode': f"{querysetOrder.postcode}",
}
    basket_items = list(map(lambda product: {
        'id': str(uuid.uuid4().fields[-1])[:5],
        'name': product.title,
        'category1': "Categor-1",
        'category2': 'Accessories',
        'itemType': 'PHYSICAL',
        'price': float(int(product.totalPrice))
    }, map_iterator))

    requestt = {
    'locale': 'tr',
    'conversationId': '123456789',
    'price': float(querysetOrder.all_total_price),
    'paidPrice': float(querysetOrder.all_total_price),
    'currency': 'TRY',
    'basketId': 'B67832',
    'paymentGroup': 'PRODUCT',
    "callbackUrl": "https://efalcollection.herokuapp.com/payment/result/",
    # "enabledInstallments": ['2', '3', '6', '9'],
    'buyer': buyer,
    'shippingAddress': address,
    'billingAddress': address,
    'basketItems': basket_items,
    # 'debitCardAllowed': True
}


    try:    
        checkout_form_initialize = iyzipay.CheckoutFormInitialize().create(requestt, options)
        content = checkout_form_initialize.read().decode('utf-8')
        json_content = json.loads(content)
        sozlukToken.append(json_content["token"])

        return HttpResponse(json_content["checkoutFormContent"] + '<div id="iyzipay-checkout-form" className="responsive"></div>') 
         

    except:
        return Response(
            {'error': 'Something went wrong when creating stripe checkout session'},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@require_http_methods(['POST'])
@csrf_exempt
def result(request):

    request = {
        'locale': 'tr',
        'conversationId': '123456789',
        'token': f"{sozlukToken[len(sozlukToken) - 1]}"
    }
    checkout_form_result = iyzipay.CheckoutForm().retrieve(request, options)

    result = checkout_form_result.read().decode('utf-8')
    sonuc = json.loads(result, object_pairs_hook=list)

    if sonuc[0][1] == 'success':
        template = settings.SITE_URL + f"?success=true&selectedOrder={orderNumber[len(orderNumber) - 1]}"
        return redirect(template)
        
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    elif sonuc[0][1] == 'failure':
        template = settings.SITE_URL + '?canceled=true'
        return redirect(template)
